#!/usr/bin/env bash

clear
manjaro_check

echo "Какой звуковой сервер вы хотите использовать?
1 - PulseAudio
2 - Pipewire(рекомендуется)"

echo -n "Выберите цифру: "
read SOUND_SERVERS

if [[ "$SOUND_SERVERS" = 1 ]]; then
    if [ $os_base="Manjaro" ]; then
        echo 'Установка PulseAudio'
        sudo pacman -Sy --needed manjaro-pulseaudio
    else
        echo "Установка PulseAudio..."
        sudo pacman -Sy --needed pulseaudio pulseaudio-alsa alsa-utils lib32-libpulse xdg-desktop-portal
        pulseaudio -D
    fi
    echo "PulseAudio успешно установлен"

    elif [[ "$SOUND_SERVERS" = 2 ]]; then
        echo "Выберите менеджер сеансов Pipewire:
        1 - pipewire-media-session(устаревший)
        2 - WirePlumber(рекомендуется)"

        read -r -p "$(tput bold)Выберите пункты (1-2): $(tput sgr0)" PIPEWIRE_SESSION_MANAGER

        if [[ $PIPEWIRE_SESSION_MANAGER =~ [1] ]]; then
            pipewire_session=pipewire-media-session
        elif [[ $PIPEWIRE_SESSION_MANAGER =~ [2] ]]; then
            pipewire_session=wireplumber
        fi

        if [ $os_base="Manjaro" ]; then
            sudo pacman -Sy manjaro-pipewire ${pipewire_session}
            systemctl disable ${pipewire_session}

            if [[ $pipewire_session="wireplumber" ]]; then
                systemctl enable --user ${pipewire_session}
            else
                systemctl enable ${pipewire_session}
            fi
        else
            sudo pacman -Sy pipewire pipewire-alsa pipewire-pulse pipewire-jack ${pipewire_session}
            systemctl disable ${pipewire_session}


            if [[ $pipewire_session="wireplumber" ]]; then
                systemctl enable --user ${pipewire_session}
            else
                systemctl enable ${pipewire_session}
            fi
        fi
fi
#!/usr/bin/env bash

chaotic_aur_check

clear

echo "Do you want to skip the basic setup (not recommended on the first run of the script)?
1 - Yes
2 - No"
read -r -p "$(tput bold)Выберите пункт (1-2): $(tput sgr0)" SKIP_BASE_INSTALL
if [[ $SKIP_BASE_INSTALL = 2 ]]; then
    
    echo "Software installation in progress..."
        
    echo "Installing components for building software with the AUR..."
    sudo pacman -Sy --needed base-devel git
        
    echo "Common software installation..."
    sudo pacman -Sy --needed vlc linux-firmware linux-headers linux-firmware-whence libxcrypt lib32-libxcrypt libxcrypt-compat lib32-libxcrypt-compat wget curl neofetch inxi flameshot squashfs-tools python-gobject python-setuptools libmtp ccache fuse3 mtpfs gvfs gvfs-mtp gpm htop
    
    echo "Установка микрокода для процесора..."
    
    cpu_model_name=$(cat /proc/cpuinfo | grep vendor | cut -c 13-24 | uniq)
    
    if [[ $cpu_model_name = "GenuineIntel" ]]; then
        sudo pacman -Sy --needed intel-ucode
    fi
    if [[ $cpu_model_name = "AuthenticAMD" ]]; then
        sudo pacman -Sy --needed amd-ucode
    fi
    
    echo "Обнаружение рабочего окружения/оконного менеджера..."
    sleep 2
    
    if [[ $DESKTOP_SESSION = "plasma" ]]; then
        echo "Installing KDE related packages..."
        sudo pacman -Sy --needed kde-system kde-utilities gwenview
        sleep 2
        
        elif [[ $DESKTOP_SESSION = "gnome" ]]; then
        echo "Installing Gnome related packages..."
        sudo pacman -Sy --needed gnome-shell-extensions gnome-tweaks gnome-disks file-roller eog ttf-roboto ttf-roboto-mono xdg-user-dirs-gtk
        sleep 2
        
        elif [[ $DESKTOP_SESSION = "xfce" ]]; then
        echo "Installing XFCE related packages..."
        sudo pacman -Sy --needed xfce4-clipman-plugin xfce4-xkb-plugin thunar-archive-plugin pavucontrol mousepad viewnior redshift
        sleep 2
        
        xfconf-query --channel thunar --property /misc-exec-shell-scripts-by-default --create --type bool --set true && thunar -q
        sleep 2
        
        elif [[ $DESKTOP_SESSION = "cinnamon" ]]; then
        echo "Installing Cinnamon related packages..."
        sudo pacman -Sy --needed xed xviewer nemo-fileroller nemo-preview nemo-terminal blueberry libcanberra xdg-dbus-proxy xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xdg-utils p7zip unrar unace lrzip squashfs-tools gst-plugins-good poppler-data mintlocale
        sleep 2
        
        elif [[ -e $HOME/.config/i3/config || $DESKTOP_SESSION = "bspwm" ]]; then
        echo "Installing i3/bspwm related packages..."
        sudo pacman -Sy --needed lightdm-gtk-greeter xorg-xrdb file-roller rofi redshift vte3 pavucontrol blueman nemo cinnamon-translations picom qview parcellite nitrogen polkit polkit-gnome
        sleep 2
        
        cp additions/picom.conf /home/$USER/.config/picom.conf
        
        echo "Writing polkit-gnome in the configuration..."
        if [ -e $HOME/.config/bspwm/bspwmrc ]; then
            sed '2a\polkit-gnome-authentication-agent-1 &' $HOME/.config/bspwm/bspwmrc
            
            elif [ -e $HOME/.config/i3/config ]; then
            echo "\\nexec --no-startup-id polkit-gnome-authentication-agent-1" >> $HOME/.config/i3/config
        fi
    fi
fi

text_editor_install(){
    echo "Select a text editor:
1 - Nano
2 - Vim
3 - Neovim
4 - Sublime Text 3
5 - VS Code(Code - OSS)
6 - VS Code(Microsoft)
7 - VS Codium
8 - JetBrains Toolbox
    ENTER to skip this step"
    
    read -r -p "$(tput bold)Select items (1-8): $(tput sgr0)" TEXT_EDITORS
    
    if [[ $TEXT_EDITORS =~ [1-8] ]]; then
        if [[ $TEXT_EDITORS =~ [1] ]]; then
            text_editor_package1=nano
        fi
        
        if [[ $TEXT_EDITORS =~ [2] ]]; then
            text_editor_package2=vim
        fi
        
        if [[ $TEXT_EDITORS =~ [3] ]]; then
            text_editor_package3=neovim
        fi
        
        if [[ $TEXT_EDITORS =~ [4] ]]; then
            text_editor_package4=sublime-text-3
        fi
        
        if [[ $TEXT_EDITORS =~ [5] ]]; then
            text_editor_package5=code
        fi
        
        if [[ $TEXT_EDITORS =~ [6] ]]; then
            text_editor_package6=visual-studio-code-bin
        fi
        
        if [[ $TEXT_EDITORS =~ [7] ]]; then
            text_editor_package7=vscodium
        fi
        
        if [[ $TEXT_EDITORS =~ [8] ]]; then
            text_editor_package8=jetbrains-toolbox
        fi
        TEXT_EDITORS_CHOICE="${text_editor_package1} ${text_editor_package2} ${text_editor_package3} ${text_editor_package4} ${text_editor_package5} ${text_editor_package6} ${text_editor_package7} ${text_editor_package8}"
        
        sudo pacman -Sy --needed $TEXT_EDITORS_CHOICE
        
        if [[ $TEXT_EDITORS_CHOICE = "" ]]; then
            :
        else
            echo "The packages are not selected!"
        fi
        
        if [[ $TEXT_EDITORS =~ [6] ]]; then
            echo "Installing additional dependencies to support Live Share in VS Code..."
            
            echo "Creating a temporary directory..."
            mkdir ~/.vsls-reqs-tmp
            cd ~/.vsls-reqs-tmp
            
            echo "Temporary directory created! Starting the download..."
            wget -O ~/vsls-reqs https://aka.ms/vsls-linux-prereq-script && chmod +x ~/vsls-reqs && ~/vsls-reqs
            
            echo "Dependencies successfully completed! Cleaning up..."
            
            cd ~
            rm -rf ~/.vsls-reqs-tmp
            
            echo "The catalog has been successfully deleted!"
            
        fi
    fi
}

messengers_install(){
    # Выбор через что можно установить мессенджеры:
    echo "Select a messenger provider:
    1 - Flatpak
    2 - Pacman.
    Some messengers are not available from the repository, Flatpak will be used for them."
    read -r -p "$(tput bold)Select items (1-2): $(tput sgr0)" MESSENGERS_INSTALLER

    if [[ $MESSENGERS_INSTALLER =~ [1-2] ]]; then
        if [[ $MESSENGERS_INSTALLER =~ [1] ]]; then
            echo "Installing Flatpak..."
            sudo pacman -Sy --needed flatpak flatpak-xdg-utils
            messenger_installer="flatpak install"
        fi
        
        echo "Select the messengers you will use:
        1 - Discord
        2 - Telegram
        3 - WhatsApp
        4 - Viber
        5 - Slack
        ENTER to skip this step"
        
        echo -n "Select numbers: "
        read MESSENGERS

        if [[ $MESSENGERS =~ [1] ]]; then
            if [ $MESSENGERS_INSTALLER =~ [1] ]; then
                messenger1="com.discordapp.Discord"
            else
                messenger1="discord"
            fi
        fi
        
        if [[ $MESSENGERS =~ [2] ]]; then
            messenger2=org.telegram.desktop
            if [ $MESSENGERS_INSTALLER =~ [1] ]; then
                messenger2="org.telegram.desktop"
            else
                messenger2="telegram-desktop"
            fi
        fi
        
        if [[ $MESSENGERS =~ [3] ]]; then
            messenger3=io.github.mimbrero.WhatsAppDesktop
            if [ $MESSENGERS_INSTALLER =~ [1] ]; then
                messenger3="io.github.mimbrero.WhatsAppDesktop"
            else
                messenger3="whatsapp-for-linux"
            fi
        fi
        
        if [[ $MESSENGERS =~ [4] ]]; then
            if [ $MESSENGERS_INSTALLER =~ [2] ]; then
                echo 'Viber is not available in Chaotic-AUR, it will be installed via flatpak'
            fi
        fi
        
        if [[ $MESSENGERS =~ [5] ]]; then
            messenger5 = com.slack.Slack
        fi

        MESSENGERS_CHOICE="${messenger1} ${messenger2} ${messenger3} ${messenger4} ${messenger5}"
        
        messenger_installer $MESSENGERS_CHOICE

        if [[ $MESSENGERS_CHOICE = "" ]]; then
            :
        elif [[ $MESSENGERS_CHOICE =~ [4] ]]; then
            flatpak install $messenger4
        else
            echo "The packages are not selected!"
        fi
    fi
}

audio_software_install(){
    echo "Installing sound processing software..."
    
    echo "Select programs:
    1 - NoiseTorch - real time noise reduction
    2 - EasyEffects - set of real time processing (only for pipewire)
    ENTER to skip this step"
    
    read -r -p "$(tput bold)Select items (1-2): $(tput sgr0)" AUDIO_SOFTWARE
    
    if [[ $AUDIO_SOFTWARE =~ [1-2] ]]; then
        if [[ $AUDIO_SOFTWARE =~ [1] ]]; then
            audio_package1=noisetorch-bin
        fi
        
        if [[ $AUDIO_SOFTWARE =~ [2] ]]; then
            audio_package2=easyeffects
        fi
        AUDIO_INSTALL_CHOICE="${audio_package1} ${audio_package2}"
        aur_helper_check
        software_module
        
        $aur_install -S $AUDIO_INSTALL_CHOICE
        
        if [[ $AUDIO_SOFTWARE = "" ]]; then
            :
        else
            echo "Пакеты не выбраны!"
        fi
    else
        typing_protection
        audio_software_install
    fi
}

browser_install(){
    echo "Installing Browsers..."
    sleep 2
    
    echo "Select a browser:
    1 - Google Chrome
    2 - Firefox
    3 - Brave
    4 - Vivaldi
    5 - Edge
    6 - Chromium
    ENTER to skip"
    
    read -r -p "$(tput bold)Select items (1-6): $(tput sgr0)"  BROWSER_SETUP
    
    if [[ "$BROWSER_SETUP" = 1 ]]; then

        sudo pacman -Sy google-chrome

    elif [[ "$BROWSER_SETUP" = 2 ]]; then

        sudo pacman -Sy firefox

    elif [[ "$BROWSER_SETUP" = 3 ]]; then

        sudo pacman -Sy brave-bin

    elif [[ "$BROWSER_SETUP" = 4 ]]; then

        sudo pacman -Sy vivaldi

    elif [[ "$BROWSER_SETUP" = 5 ]]; then

        sudo pacman -Sy microsoft-edge-stable-bin

    elif [[ "$BROWSER_SETUP" = 6 ]]; then

        sudo pacman -Sy chromium

    fi
}

torrent_install(){
    echo "Installing the Bittorrent client..."
    
    echo "Choose a client:
    1 - qBitTorrent
    2 - Transmission
    ENTER to skip"
    
    read -r -p "$(tput bold)Select items (1-2): $(tput sgr0)" TORRENT_SETUP
    
    if [[ "$TORRENT_SETUP" = 1 ]]; then

        sudo pacman -Sy qbittorrent

        elif [[ "$TORRENT_SETUP" = 2 ]]; then
            if [[ $DESKTOP_SESSION = "plasma" ]]; then

                sudo pacman -Sy transmission-qt

                elif  [[ $DESKTOP_SESSION = "gnome" || $DESKTOP_SESSION = "xfce" || $DESKTOP_SESSION = "cinnamon" ]]; then

                sudo pacman -Sy transmission-gtk

            fi
        fi
    fi
}

multimedia_install(){
    echo "Select items
    1 - DaVinci Resolve Studio
    2 - Kdenlive
    3 - OBS Studio
    4 - SimpleScreenRecorder
    5 - Ardour
    6 - Audacity
    7 - Krita
    8 - GIMP
    9 - DigiKam
    10 - PiTiVi"

    read -r -p "$(tput bold)Select items (1-10): $(tput sgr0)" MULTIMEDIA_SETUP

    if [[ $MULTIMEDIA_SETUP =~ [1-9] ]]; then
        if [[ $MULTIMEDIA_SETUP =~ [1] ]]; then
            multimedia_package1=davinci-resolve
        fi
        
        if [[ $MULTIMEDIA_SETUP =~ [2] ]]; then
            multimedia_package2=kdenlive
        fi

        if [[ $MULTIMEDIA_SETUP =~ [3] ]]; then

            read -r -p "Do you want to install plugins for OBS? [Y/n] " obs_plugin_install

            if [[ $obs_plugin_install =~ ^([yY][eE][sS]|[yY])$ ]]; then
                echo 'Select plugins
                1 - obs-gstreamer
                2 - obs-vkcapture'

                read -r -p "$(tput bold)Select items (1-2): $(tput sgr0)" OBS_PLUGINS

                if [[ $OBS_PLUGINS =~ [1-2] ]]; then
                    if [[ $OBS_PLUGINS =~ [1] ]]; then
                        obs_plugin1=com.obsproject.Studio.Plugin.Gstreamer
                    fi

                    if [[ $OBS_PLUGINS =~ [2] ]]; then
                        obs_plugin2=com.obsproject.Studio.Plugin.OBSVkCapture
                        obs_plugin3=org.freedesktop.Platform.VulkanLayer.OBSVkCapture
                    fi

                    OBS_PLUGINS_INSTALL="${obs_plugin1} ${obs_plugin2} ${obs_plugin3}"
                fi    

            else
                :
            fi
        fi

        if [[ $MULTIMEDIA_SETUP =~ [4] ]]; then
            multimedia_package4=simplescreenrecorder
        fi

        if [[ $MULTIMEDIA_SETUP =~ [5] ]]; then
            multimedia_package5=ardour
        fi

        if [[ $MULTIMEDIA_SETUP =~ [6] ]]; then
            multimedia_package6=audacity
        fi

        if [[ $MULTIMEDIA_SETUP =~ [7] ]]; then
            multimedia_package7=krita
        fi

        if [[ $MULTIMEDIA_SETUP =~ [8] ]]; then
            multimedia_package8=gimp
        fi

        if [[ $MULTIMEDIA_SETUP =~ [9] ]]; then
            multimedia_package9=digikam
        fi

        if [[ $MULTIMEDIA_SETUP =~ [10] ]]; then
            multimedia_package10=pitivi
        fi

        MULTIMEDIA_INSTALL_CHOICE="${multimedia_package1} ${multimedia_package2} ${multimedia_package4} ${multimedia_package5} ${multimedia_package6} ${multimedia_package7} ${multimedia_package8} ${multimedia_package9} ${multimedia_package10}"
        aur_helper_check
        software_module
        
        $aur_install -S $MULTIMEDIA_INSTALL_CHOICE

        if [[ $MULTIMEDIA_SETUP =~ [3] ]]; then
            echo "OBS will be installed via Flatpak"
            flatpak install com.obsproject.Studio

            if [[ $obs_plugin_install = [Nn][Oo]|[No] ]]; then
                :
            else
                echo "Installing plugins..."
                flatpak install $OBS_PLUGINS_INSTALL
            fi
        fi
        
        if [[ $MULTIMEDIA_SETUP = "" ]]; then
            :
        else
            echo "The packages are not selected!"
        fi
    else
        typing_protection
        audio_software_install
    fi
}

office_suite_install(){
    echo "Select option.
    1 - LibreOffice
    2 - OpenOffice
    3 - OnlyOffice
    4 - WPS Office"

    read -r -p "$(tput bold)Select item (1-4): $(tput sgr0)" OFFICE_SUITE

    if [[ $OFFICE_SUITE =~ [1] ]]; then
        sudo pacman -Sy libreoffice
    elif [[ $OFFICE_SUITE =~ [2] ]]; then
        aur_helper_check
        $aur_install -S openoffice-bin
    elif [[ $OFFICE_SUITE =~ [3] ]]; then
        aur_helper_check
        $aur_install -S onlyoffice-bin
    elif [[ $OFFICE_SUITE =~ [4] ]]; then
        aur_helper_check
        $aur_install -S wps-office ttf-wps-office
    fi
}

player_install(){
    echo "Select type:
    1 - Video player
    2 - Music player
    ENTER to skip"

    read -r -p "$(tput bold)Select item (1-2): $(tput sgr0)" PLAYER_TYPE

    if [[ $PLAYER_TYPE =~ [1] ]]; then
        echo "Select the item:
        1 - VLC Media Player
        2 - Kodi
        3 - Miro
        4 - SMPlayer
        5 - MPV"

        read -r -p "$(tput bold)Select item (1-5): $(tput sgr0)" VIDEO_PLAYER

        if [[ $VIDEO_PLAYER =~ [1] ]]; then
            sudo pacman -Sy vlc
        elif [[ $VIDEO_PLAYER =~ [2] ]]; then
            sudo pacman -Sy kodi
        elif [[ $VIDEO_PLAYER =~ [3] ]]; then
            aur_helper_check
            $aur_install -S mira
        elif [[ $VIDEO_PLAYER =~ [4] ]]; then
            sudo pacman -Sy smplayer
        elif [[ $VIDEO_PLAYER =~ [5] ]]; then
            echo "Do you want to use a Front-end (shell) for MPV?"

            read -r -p "$(tput bold)Select item (Y\n): $(tput sgr0)" MPV_FRONT

            if [[ $MPV_FRONT =~ "" ]]; then
                echo "Select Item:
                1 - Celluloid
                2 - Haruna"

                read -r -p "$(tput bold)Select item (1-2): $(tput sgr0)" MPV_FRONT_INSTALL

                if [[ $MPV_FRONT_INSTALL =~ [1] ]]; then
                    sudo pacman -Sy celluloid
                elif [[ $MPV_FRONT_INSTALL =~ [2] ]]; then
                    sudo pacman -Sy haruna
                fi
            else
                sudo pacman -Sy mpv
            fi
        fi
    elif [[ $PLAYER_TYPE =~ [2] ]]; then
        echo "Select Item:
        1 - VLC Media Player
        2 - Amarok
        3 - Spotify
        4 - Audacious"

        read -r -p "$(tput bold)Select item (1-2): $(tput sgr0)" MUSIC_PLAYER

        if [[ $MUSIC_PLAYER =~ [1] ]]; then
            sudo pacman -Sy vlc
        elif [[ $MUSIC_PLAYER =~ [2] ]]; then
            aur_helper_check
            $aur_install -S amarok
        elif [[ $MUSIC_PLAYER =~ [3] ]]; then
            aur_helper_check
            $aur_install -S Spotify
        elif [[ $MUSIC_PLAYER =~ [4] ]]; then
            sudo pacman -Sy audacious
        fi
    fi
}

PS3="Select the menu option: "

select start in Execute\ the\ entire\ script Choose\ what\ you\ need; do
    case $start in
        Execute\ the\ entire\ script)
            text_editor_install
            messengers_install
            audio_software_install
            browser_install
            torrent_install
            multimedia_install
            office_suite_install
            player_install
        ;;
        
        Choose\ what\ you\ need)
            select software_install in Install\ text\ editors Install\ messengers Install\ audio\ software Install\ browser Install\ Bittorrent-client Install\ multimedia\ software Install\ Office\ Suite Install\ audio/video\ players Exit; do
                case $software_install in
                    Install\ text\ editors)
                        text_editor_install
                    ;;
                    
                    Install\ messengers)
                        messengers_install
                    ;;
                    
                    Install\ audio\ software)
                        audio_software_install
                    ;;
                    
                    Install\ browser)
                        browser_install
                    ;;
                    
                    Install\ Bittorrent-client)
                        torrent_install
                    ;;

                    Install\ multimedia\ software)
                        multimedia_install
                    ;;
                    
                    Install\ Office\ Suite)
                        office_suite_install
                    ;;

                    Install\ audio/video\ players)
                        player_install
                    ;;

                    Выход)
                        exit
                    ;;
                esac
            done
        ;;
    esac
done
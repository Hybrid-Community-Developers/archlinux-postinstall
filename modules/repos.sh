#!/usr/bin/env bash
clear

manjaro_check

echo "Configuring repositories..."

echo "Updating the keyring..."

keyring_update(){
    if [ $os_base="Manjaro" ]; then
        sudo pacman-key --init
        
        sudo pacman -Sy archlinux-keyring manjaro-keyring
        
        sudo pacman-key --populate archlinux manjaro
    else
        sudo pacman-key --init
        
        sudo pacman -Sy archlinux-keyring
        
        sudo pacman-key --populate archlinux
    fi
}


echo "Upgrading the system..."
sudo pacman -Syu

echo "The keychain update was successful! Adding repositories..."


echo "Adding the Multilib repository..."

if [ $os_base="Manjaro" ]; then
    echo "Multilib already added!"
else
    if grep "[[]" /etc/pacman.conf | grep "multilib" | grep -v "multilib-testing" | grep -v "#" > /dev/null; then
        echo "Multilib already added!"
    else
        sudo sed -i '$ a \\n[multilib]\nInclude = /etc/pacman.d/mirrorlist' /etc/pacman.conf
    fi
fi

echo "Multilib has been successfully added! Adding the Chaotic-AUR repository"

if grep "chaotic-aur" /etc/pacman.conf; then
    echo "Chaotic-AUR has already been added!"
else
    echo "Adding a keyring..."
    
    sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
    sudo pacman-key --lsign-key FBA220DFC880C036
    
    echo "Adding new mirrors and the keyring..."
    sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
    
    echo "Adding a repository to pacman.conf"
    sudo sed -i '$ a \\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist' /etc/pacman.conf

    echo "Chaotic-AUR has been succefully added!"
fi
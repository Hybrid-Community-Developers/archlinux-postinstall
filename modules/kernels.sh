#!/usr/bin/env bash

nvidia_driver_check

clear

liquorix_install(){
    echo "Adding a liquorix repository..."

    sudo pacman-key --keyserver hkps://keyserver.ubuntu.com --recv-keys 9AE4078033F8024D
    sudo pacman-key --lsign-key 9AE4078033F8024D

    sudo sed -i '$ a \\n[liquorix]\nServer = https://liquorix.net/archlinux/$repo/$arch' /etc/pacman.conf

    echo "The Liquorix repository has been successfully added!"
    sleep 2

    sudo pacman -Sy linux-lqx linux-lqx-headers
}

xanmod_install(){
    chaotic_aur_check

    echo "Select the kernel version:
    1 - XanMod-LTS
    2 - XanMod-Edge"

    read -r -p "$(tput bold)Select the item (1-2): $(tput sgr0)" XANMOD_TYPE

    if [[ $XANMOD_TYPE = [1] ]]; then
        sudo pacman -Sy linux-xanmod-lts linux-xanmod-lts-headers
    fi

    if [[ $XANMOD_TYPE = [2] ]]; then
        sudo pacman -Sy linux-xanmod-edge linux-xanmod-edge-headers
    fi
}

tkg_install(){
    echo "Select a version:
    Latest versions.
    1 - TKG-CFS
    2 - TKF-PDS
    3 - TKG-BMQ

    LTS-versions (Long-Term Support versions)
    4 - TKG-CFS-LTS
    5 - TKG-PDS-LTS
    6 - TKG-BMQ-LTS"

    read -r -p "$(tput bold)Select the item (1-6): $(tput sgr0)" TKG_TYPE

    if [[ $TKG_TYPE =~ [1] ]]; then
        sudo pacman -S linux-tkg-cfs linux-tkg-cfs-headers
    fi

    if [[ $TKG_TYPE =~ [2] ]]; then
        sudo pacman -S linux-tkg-pds linux-tkg-pds-headers
    fi

    if [[ $TKG_TYPE =~ [3] ]]; then
        sudo pacman -S linux-tkg-bmq linux-tkg-bmq-headers
    fi

    if [[ $TKG_TYPE =~ [4] ]]; then
        sudo pacman -S linux-lts-tkg-cfs linux-lts-tkg-cfs-headers
    fi

    if [[ $TKG_TYPE =~ [5] ]]; then
        sudo pacman -S linux-lts-tkg-pds linux-lts-tkg-pds-headers
    fi

    if [[ $TKG_TYPE =~ [6] ]]; then
        sudo pacman -S linux-lts-tkg-bmq linux-lts-tkg-bmq-headers
    fi
}

cachy_install(){
    echo "Adding a cachyos repository"
    echo "Warning! This repository may conflict with chaotic-aur, you do so at your own risk!"

    read -r -p "$(tput bold)Continue? (Y\n): $(tput sgr0)" cachy_repo_add

    if [[ $cachy_repo_add =~ [Yy][Es][Ss]|[Yy] ]]; then
        sudo pacman-key --recv-keys F3B607488DB35A47 --keyserver keyserver.ubuntu.com
        sudo pacman-key --lsign-key F3B607488DB35A47
        sudo pacman -U 'https://mirror.cachyos.org/repo/x86_64/cachyos/cachyos-keyring-2-1-any.pkg.tar.zst' 'https://mirror.cachyos.org/repo/x86_64/cachyos/cachyos-mirrorlist-10-1-any.pkg.tar.zst' 'https://mirror.cachyos.org/repo/x86_64/cachyos/cachyos-v3-mirrorlist-10-1-any.pkg.tar.zst'

        if [[ $(/lib/ld-linux-x86-64.so.2 --help | grep "x86-64-v3 (supported, searched)") ]]; then
            sudo sed -i 's/Architecture = auto/Architecture = x86_64 x86_64_v3/g'

            sudo sh -c 'echo "\n[cachyos-v3]\\nInclude = /etc/pacman.d/cachyos-v3-mirrorlist" >> /etc/pacman.conf'
        else
            sudo sh -c 'echo "\n[cachyos]\\nInclude = /etc/pacman.d/cachyos-mirrorlist" >> /etc/pacman.conf'
        fi

        echo "Updating the repository list..."
        sudo pacman -Syu

        clear

        echo "Select the kernel type:
        1 - BMQ
        2 - BORE
        3 - CacULE
        4 - CFS
        5 - Hardened
        6 - PDS
        7 - RC
        8 - TT"

        read -r -p "$(tput bold)Select the item (1-8): $(tput sgr0)" CACHY_TYPE

        if [[ $CACHY_TYPE =~ [1] ]]; then
            sudo pacman -Sy linux-cachyos-bmq linux-cachyos-bmq-headers
        elif [[ $CACHY_TYPE =~ [2] ]]; then
            sudo pacman -Sy linux-cachyos-bore linux-cachyos-bore-headers
        elif [[ $CACHY_TYPE =~ [3] ]]; then
            sudo pacman -Sy linux-cachyos-cacule linux-cachyos-cacule-headers
        elif [[ $CACHY_TYPE =~ [4] ]]; then
            sudo pacman -Sy linux-cachyos-cfs linux-cachyos-cfs-headers
        elif [[ $CACHY_TYPE =~ [5] ]]; then
            sudo pacman -Sy linux-cachyos-hardened linux-cachyos-hardened-headers
        elif [[ $CACHY_TYPE =~ [6] ]]; then
            sudo pacman -Sy linux-cachyos-pds linux-cachyos-pds-headers
        elif [[ $CACHY_TYPE =~ [7] ]]; then
            sudo pacman -Sy linux-cachyos-rc linux-cachyos-rc-headers
        elif [[ $CACHY_TYPE =~ [8] ]]; then
            sudo pacman -Sy linux-cachyos-tt linux-cachyos-tt-headers
        fi
    else
        exit
    fi
}

custom_kernel_install(){
    echo "Installing custom kernels..."

    echo "Select a kernel:
    1 - Linux-zen
    2 - Linux-lqx
    3 - Linux-xanmod-*
    4 - Linux-tkg-*
    5 - Linux-cachyos-*"

    read -r -p "$(tput bold)Select the item (1-4): $(tput sgr0)" KERNEL_CHOICE

    nvidia_driver_check

    if [[ $KERNEL_CHOICE =~ [1] ]]; then
        sudo pacman -Sy --needed linux-zen linux-zen-headers
    elif [[ $KERNEL_CHOICE =~ [2] ]]; then
        if [[ $nv_arch == Tesla || $nv_arch == Fermi ]]; then
            echo "Your video driver does not support the Liquorix kernel"
        elif [[ $nv_arch == Kepler || $nv_arch == Maxwell || $nv_arch == Pascal || $nv_arch == Volta || $nv_arch == Turing || $nv_arch == Ampere ]]; then
            liquorix_install
        elif [[ $(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p') || $(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p') ]]; then
            liquorix_install
        fi
    elif [[ $KERNEL_CHOICE =~ [3] ]]; then
        if [[ $nv_arch == Tesla || $nv_arch == Fermi ]]; then
            echo "Your video driver does not support the XanMod kernel"
        elif [[ $nv_arch == Kepler || $nv_arch == Maxwell || $nv_arch == Pascal || $nv_arch == Volta || $nv_arch == Turing || $nv_arch == Ampere ]]; then
            xanmod_install
        elif [[ $(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p') || $(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p') ]]; then
            xanmod_install
        fi
    elif [[ $KERNEL_CHOICE =~ [4] ]]; then
        if [[ $nv_arch == Tesla || $nv_arch == Fermi ]]; then
            echo "Your video driver does not support the TKG kernel"
        elif [[ $nv_arch == Kepler || $nv_arch == Maxwell || $nv_arch == Pascal || $nv_arch == Volta || $nv_arch == Turing || $nv_arch == Ampere ]]; then
            tkg_install
        elif [[ $(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p') || $(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p') ]]; then
            tkg_install
        fi
    elif [[ $KERNEL_CHOICE =~ [5] ]]; then
        if [[ $nv_arch == Tesla || $nv_arch == Fermi ]]; then
            echo "Your video driver does not support the CachyOS kernel"
        elif [[ $nv_arch == Kepler || $nv_arch == Maxwell || $nv_arch == Pascal || $nv_arch == Volta || $nv_arch == Turing || $nv_arch == Ampere ]]; then
            cachy_install
        elif [[ $(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p') || $(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p') ]]; then
            cachy_install
        fi
    fi
    
}

custom_kernel_install

echo "Updating initramfs..."
sleep 2
sudo mkinitcpio -P

echo "Updating GRUB entries..."
sleep 2
sudo grub-mkconfig -o /boot/grub/grub.cfg
#!/usr/bin/env bash 

clear

echo "Configuring Services..."

echo "Turning on the Bluetooth service"

# Проверка есть ли файл bluetooth.service в /etc/systemd/system/ и есть в /proc/modules есть модуль btusb, то запустить службу bluetooth.service
if [ -f "/etc/systemd/system/bluetooth.service" ] && grep -q "btusb" /proc/modules; then
    echo "Bluetooth.service is already installed!"
    echo "Enabling a bluetooth.service..."
    systemctl enable --now bluetooth.service
else
    echo "Installing bluetooth.service..."
    sudo pacman -S bluez bluez-utils bluez-tools
    echo "Enabling a bluetooth.service..."
    systemctl enable --now bluetooth.service
fi

echo "Disabling unnecessary services..."

if [[ $DESKTOP_SESSION = "plasma" ]]; then
    echo "Indexing has been successfully disabled!"

    systemctl --user mask kde-baloo.service plasma-baloorunner.service

    echo "Indexing has been successfully disabled!"

    elif [[ $DESKTOP_SESSION = "gnome" ]]; then
        echo "Disabling the Gnome Tracker..."

        systemctl --user mask tracker-miner-apps tracker-miner-fs tracker-store

        echo "Select the services you want to disable:
        1 - GNOME Integration Service with Wacom Graphics Tablet.
        2 - GNOME Print Notification Service.
        3 - Color Profile Management Service - The Redshift equivalent of GNOME.
        4 - System Special Features Management Service - (!)Do not disable for people with disabilities.
        5 - The Wireless Internet Connection Management Service. Do not disable for notebook users!
        6 - Protect against unauthorized USB devices when locking the screen.
        7 - Screen lock settings.
        8 - File and directory sharing settings.
        9 - Service controlling rfkill subsystem. This service is responsible for Flying Mode.
        10 - GNOME Keyboard and Layout Management. If you have your layouts set up, you can turn them off.
        11 - GNOME integration with the map reader.
        12 - Disk Space Tracking Service."

        echo -n "Введите цифры: "
        read GNOME_SERVICES

        GNOME_SERVICES=0

        if [[ $GNOME_SERVICES =~ [1] ]]; then
            gnome_service1=org.gnome.SettingsDaemon.Wacom.service
        fi

        if [[ $GNOME_SERVICES =~ [2] ]]; then
            gnome_service2=org.gnome.SettingsDaemon.PrintNotifications.service
        fi

        if [[ $GNOME_SERVICES =~ [3] ]]; then
            gnome_service3=org.gnome.SettingsDaemon.Color.service
        fi            
        if [[ $GNOME_SERVICES =~ [4] ]]; then
            gnome_service4=org.gnome.SettingsDaemon.A11ySettings.service
        fi
        if [[ $GNOME_SERVICES =~ [5] ]]; then
            gnome_service5=org.gnome.SettingsDaemon.Wwan.service
        fi
        if [[ $GNOME_SERVICES =~ [6] ]]; then
            gnome_service6=org.gnome.SettingsDaemon.UsbProtection.service
        fi                    
        if [[ $GNOME_SERVICES =~ [7] ]]; then
            gnome_service7=org.gnome.SettingsDaemon.ScreensaverProxy.service
        fi
        if [[ $GNOME_SERVICES =~ [8] ]]; then
            gnome_service8=org.gnome.SettingsDaemon.Sharing.service
        fi
        if [[ $GNOME_SERVICES =~ [9] ]]; then
            gnome_service9=org.gnome.SettingsDaemon.Rfkill.service
        fi
        if [[ $GNOME_SERVICES =~ [10] ]]; then
            gnome_service10=org.gnome.SettingsDaemon.Keyboard.service
        fi
        if [[ $GNOME_SERVICES =~ [11] ]]; then
            gnome_service11=org.gnome.SettingsDaemon.Smartcard.service
        fi
        if [[ $GNOME_SERVICES =~ [12] ]]; then
            gnome_service12=org.gnome.SettingsDaemon.Housekeeping.service
        fi

        GNOME_SERVICES="${gnome_service1} ${gnome_service2} ${gnome_service3} ${gnome_service4} ${gnome_service5} ${gnome_service6} ${gnome_service7} ${gnome_service8} ${gnome_service9} ${gnome_service10} ${gnome_service11} ${gnome_service12}"

        if [[ $GNOME_SERVICES = 0 ]]; then
            :
        else
            systemctl --user mask ${GNOME_SERVICES}
        fi
else
    echo "Your shell does not require you to disable unnecessary services!"
fi

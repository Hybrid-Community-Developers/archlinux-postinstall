#!/usr/bin/env bash

manjaro_check

clear

echo "Adjusting mirrors..."

manjaro_pacman-mirrors(){
    sudo pacman-mirrors --geoip --fasttrack
    sudo pacman -Sy
}
arch_pacman-mirrors(){
    sudo pacman -Sy --needed reflector

    sudo reflector --verbose --protocol https --latest 25 --sort rate --save /etc/pacman.d/mirrorlist
    sudo pacman -Sy
}

if [ $os_base="Manjaro" ]; then
    manjaro_pacman-mirrors
else
    arch_pacman-mirrors
fi
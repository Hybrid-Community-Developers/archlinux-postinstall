#!/usr/bin/env bash

clear 

echo "What software do you want to install to run games?
1 - Steam
2 - StartWine
3 - PortProton
4 - Wine-TKG
5 - GVM - Utility for viewing load in games"

echo -n "Enter numbers: "
read GAMES_LAUNCHERS

if [[ $GAMES_LAUNCHERS =~ [1] ]]; then
    echo "Installing Steam..."
    sudo pacman -Sy steam-native-runtime steam
    sleep 2

    echo "Steam succefully installed!"
fi

if [[ $GAMES_LAUNCHERS =~ [2] ]]; then
    "Installing StartWine... Creating a temporary directory..."

    mkdir ~/.sw_tmp
    cd ~/.sw_tmp

    echo "Catalog successfully created! The installer starts downloading..."
    curl -s https://api.github.com/repos/RusNor/StartWine-Launcher/releases/latest | grep "StartWine_v" | cut -d : -f 2,3 | tr -d \" | wget -qi -

    echo "The installer is loaded! Running..."
    chmod +x StartWine_v* && sh StartWine_v*

    echo "Installation successfully completed! Cleaning..."
    cd ~
    rm -rf ~/.sw_tmp
fi

if [[ $GAMES_LAUNCHERS =~ [3] ]]; then
    echo "Installing PortProton... Creating a temporary directory..."

    mkdir ~/.pp_tmp
    cd ~/.pp_tmp

    echo "Catalogue well done! Package download..."
    wget https://github.com/Castro-Fidel/PortProton_PKGBUILD/releases/download/portproton-1.0-8-x86_64/portproton-1.0-8-x86_64.pkg.tar.zst

    echo "Installing a package..."
    sudo pacman -U portproton-1.0-8-x86_64/portproton-1.0-8-x86_64.pkg.tar.zst

    echo "Package installed successfully! Cleaning..."

    cd ~
    rm -rf ~/.pp_tmp
fi

if [[ $GAMES_LAUNCHERS =~ [4] ]]; then
    echo "Select a branch:
    1 - Stable
    2 - Staging"
    read -p "Введите цифру: " WINE_TKG_BRANCH
    if [[ $WINE_TKG_BRANCH =~ [1] ]]; then
        sudo pacman -Sy wine-tkg-fsync-git winetricks
    elif [[ $WINE_TKG_BRANCH =~ [2] ]]; then
        sudo pacman -Sy wine-tkg-staging-fsync-git winetricks
    fi
fi

if [[ $GAMES_LAUNCHERS =~ [5] ]]; then
    echo "Installing the GVM..."

    mkdir ~/.gmv_tmp
    cd ~/.gmv_tmp
    
    echo "Catalog successfully created! The installer starts downloading..."
    curl -s https://api.github.com/repos/RusNor/Ported-version-of-MangoHud-and-vkBasalt/releases/latest | grep "install_gmv_utils_v" | cut -d : -f 2,3 | tr -d \" | wget -qi -

    echo "The installer is loaded! Running..."
    chmod +x install_gmv_utils_v* && sh install_gmv_utils_v*

    echo "Installation successfully completed! Cleaning..."
    cd ~
    rm -rf ~/.gmv_tmp
fi
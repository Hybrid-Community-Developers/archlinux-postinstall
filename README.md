# ArchLinux-PostInstall

Script for the initial post-installation of Arch Linux

---

## Benefits

* Modular system

  * Repositories

    * Multiib activation
    * Adding Chaotic-AUR

  * Mirrors
    * Quick mirrors search by region
    * Search for fast mirrors worldwide
  * Optimization
    * Installing and activating services:
        **Ananicy-cpp**
        **Nohang**
        **IrqBalance**
        **RNG-Tools**
        **Dbus-Broker**
        **Preload**
      **TRIM** enable **TRIM** on SSDs
      **Installing Mesa-TKG-git and configuring SAM on AMD Radeon video cards
    * Configuring Input/Output scheduler depending on SSDs
    * Configuring kernel parameters in GRUB for faster booting
    * Optimizing package builds via **makepkg**
    * Customizing Clang compiler by default, by user request
    * Installing ZRam
  * Setting up services
    * Turning on Bluetooth, if any
    * Disabling unnecessary services in the shells
      * KDE
        * Indexing files
      * Gnome
        * Tracker
        * GNOME integration service with Wacom graphics tablet
        * GNOME Printing Notification Service
        * Color Profile Management Service
        * Manage System Specific Capabilities Service
        * Manage Wireless Internet Connections Service
        * Lock screen protection from unauthorized USB devices
        * File and directory sharing setup service
        * Rfkill subsystem management service
        * GNOME Keyboard and Layout Management Service
        * GNOME integration with the map reader
        * Disk Space Tracking Service
  * Software Installation
    * Components for assembling with the AUR
    * General Software
    * Automatic microcode installation for the processor
    * Automatic installation of shell packages
      * KDE Plasma
      * Gnome
      * XFCE
      * Cinnamon
      * i3wm\bspwm
        * PolKit automatic tuning for them
      ---
    * Installing text editors
      * Nano
      * Vim
      * Neovim
      * Sublime Text 3
      * VS Code(Code - OSS)
      * VS Code(Microsoft)
      * VS Codium
      * JetBrains Toolbox
      ---
    * Installing messengers through Flatpak
      * Discord
      * Telegram
      * WhatsApp
      * Viber
      * Slack
      ---
    * Audio software installation
      * NoiseTorch
      * EasyEffects
      ---
    * Browser installation
      * Google Chrome
      * Firefox
      * Brave
      * Vivaldi
      * Microsoft Edge
      * Chromium
      ---
    * Installing the Torrent client
      * qBittorrent
      * Transmission(GTK/Qt)
      ---
    * Multimedia software installation
      * DaVinci Resolve
      * Kdenlive
      * OBS Studio
      * SimpleScreenRecorder
      * Ardour
      * Audacity
      * Krita
      * GIMP
      * DigiKam
      * PiTiVi
      ---
    * Office software
      * LibreOffice
      * OpenOffice
      * OnlyOffice
      * WPS Office
      ---
    * Players
      * Video players
        * VLC Media Player
        * Kodi
        * Miro
        * SMPlayer
        * MPV
          * Frontends
            * Haruna
            * Celluloid
      * Audio players
        * VLC Media Player
        * Amarok
        * Spotify
        * Audacious
      ---
  * AUR
    * Console helpers
      * Yay - Yet another yogurt
      * Pikaur
      * Pacaur
      * Pakku
      * Trizen
      * Paru
    * Installing Pamac
  * Installing the Drivers
    * Installing video drivers
      * Nvidia
        * Installing version depending on video chip architecture
        * Configuring mkinitcpio\GRUB modules
        * Hybrid Graphics setup (in testing)
      * AMD
        * GCN 1.0 and GCN 2.0 architecture configuration
      * Intel
  * Installation of custom kernels
    * Linux-zen
    * Checking for the driver in use
      * Linux-liquorix
      * Linux-XanMod
      * Linux-TKG
      * Linux-CachyOS
  * ZSH setup(in Rewrite)
    * Installing Oh-My-Zsh
    * Installing **zsh-syntax-highlighting** and **zsh-autosuggestions**
    * Installing the Powerlevel10k theme
    *Selecting a sound server
    * PulseAudio
    * Pipewire
      * pipewire-media-session
      * WirePlumber
  * Game lunchers
    * Steam
    * PortProton
    * StartWine
    * Wine-TKG
    * GMV - Utility for viewing load in games
  * Font configuration
    * Font anti-aliasing configuration
    * Installing frequently used fonts
      * Noto Font
      * TTF-Liberation
    * Choice of additional fonts
      * Ubuntu Font Family
      * Roboto Font Family
      * Iosevka Icons Nerd
      * Awesome Icons
      * Icomoon Feather Icons
      * Material Icons

---

### Usage

```bash
sudo pacman -Sy git --needed && git clone https://codeberg.org/Hybrid-Community-Developers/archlinux-postinstall.git /home/$USER/archlinux-postinstall && cd /home/$USER/archlinux-postinstall/ && sh post-setup.sh
```

---

#### Development Help

If you want to help with development:

* Make a fork
* Make your own changes
* Create a merge request

Or post in Issues your suggestions for the script.

Thanks for your help:

* <https://codeberg.org/kamiya>

* <https://codeberg.org/Elifian>

* <https://codeberg.org/Lintech>

---

##### Changelog

v1.3.0

* Planned
  * Make language detection

---

v1.2.0

* Addedd
  * Compatibility with Manjaro
  * Setting up SELinux
  * English translation of the script
  * Software
    * Added htop to common software
    * Changed lxqt-polkit to polkit-gnome for i3\bspwm

---

v1.1.0

* Added
  * Multimedia and Office suite section, Video player to "Software" section
  * Cachy-os kernel in "kernels" section
  * Code optimization
    * Added ``functions_helper.sh`` to optimize function usage
    * Improved AUR helper detection
    * Moved variable ``aur_install`` to ``aur.sh``.
    * Added check for chaotic-aur repository presence
  * Option for amdgpu.dc=1 to fix HDMI audio on some graphics cards
  * Preload to optimization services
  
* Fixed
  * Detecting Radeon graphics cards with GCN 1\2 architectures, now chips are detected correctly

---

v1.0.14

* Fixed
  * Faster optimization menu loading
  * Optimization and Drivers menu load is fixed.

---

v1.0.13

* Fixed
  * Removed ``/usr/bin/env bash`` due to errors with launching modules.(False alarm, the error was due to launching through a folder)

---

v1.0.12

* Added
  * Loaded the latest StartWine release
  * GMV utility from StartWine creator

---

v1.0.11

* Added
  * AMDGPU configuration for GCN 1.0 and GCN 2.0 video cards
  * Additional packages for mesa-tkg-git
  * Additional packages for AMD and Intel in Drivers section

* Fixed
  * Installing packages for AMDGPU

---

v1.0.10

* Added
  * ZRam compression selection
    * lzo
    * lz4
    * zstd
  * Mesa-TKG installation
  * Installing Wine-TKG
  * Using `#!/usr/bin/env bash` instead of `#!/bin/bash`.
  * Enabling "Smart Access Memory" mode for Radeon video cards
  * Adding lpj value to GRUB

* Fixed
  * Adjusting makepkg.conf
  * Adjusted ZSH
    * Added additional checks.
  * Installing modules for optimization
  * Installing messengers
    * Added option to install via pacman or Flatpak
  * I/O scheduler setup
  * Changed ZRam configuration to use `zram-generator` instead of udev rules
  * Corrected README link
  * Getting lpj

---

v1.0.9

* Added
  * Checked for radeon driver usage, and skipped driver installation
  * Changed ZSH installation logic
  * Changed service setup logic

* Fixed
  * Clang/LLVM packages

---

v1.0.8

* Added
  * Hybrid graphics configuration for Nvidia cards (in test mode)
  * Added choice to add Clang\LLVM by default in makepkg

* Removed
  * Changelog inside script in favor of ``README

* Fixed
  * Nvidia packages for Fermi architecture
  * Selecting Clang in the optimization menu
  * ZRam tuning

---

v1.0.7

* Added
  * Writing startup parameters in GRUB
  * Writing flags in makepkg.conf
  * Writing Nvidia modules in mkinitcpio.conf and GRUB

* Fixed
  * Missing function for makepkg under "Execute entire script" in optimization menu
  * Fixed disabling unnecessary services in Gnome
  * Added initramfs generation for Nvidia drivers
  * Fixed lxpolkit to lxqt-policykit-agent under "software"

---

v1.0.6

* Added
  * Country selection in mirror setup

* Fixed
  * I/O scheduler setup (using printf instead of echo | sudo tee)
  * ZRam installation
  * ZSH installation

* Removed unnecessary sleep to speed up code

---

v1.0.5

* Added
  * Nothing, mostly bug fixes

* Fixed
  * Installing custom drivers
  * Detecting drivers for Nvidia

---

v1.0.4

* Added
  * Added udev rule for I/O scheduler
  * Updated "Optimization" menu
  * Completely unlinked from config

---

v1.0.3

* Added
  * Rewritten drivers installation logic
  * Removed the config
  
* Fixed
  * Drivers installation menu

---

v1.0.2

* Fixed
  * Critical error with non opening menu
  * Improved font installation

---

v1.0.0 - First Release

* Added.
  * Detection of the desktop shell and/or window manager in use for basic software installation
  * Detection of i3/bspwm by searching their configs
  * Programs for sound processing in the "Software" section
  * If there is no required value in ``postinstall.conf``, the required module is called
  * The variable ``DE_WM_SESSION`` was removed from the config in favor of the XDG variable ``$DESKTOP_SESSION``
  * Skip installation of the base software
  * Initialized transition to select multiple programs in the menu

* Fixed
  * Root partition detection to activate TRIM on SSD drives
  * Fixed microcode installation for Intel/AMD processors
  * Removed --noconfirm from pacman related commands

---
Copyright by vellynproduction

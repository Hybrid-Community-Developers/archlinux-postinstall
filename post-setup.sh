#!/usr/bin/env bash

source ./scripts/functions_helper

cas(){
    clear
    REPLY=
    echo "
▄▀█ █▀█ █▀▀ █░█ ▄▄ █▀█ █▀█ █▀ ▀█▀ █ █▄░█ █▀ ▀█▀ ▄▀█ █░░ █░░
█▀█ █▀▄ █▄▄ █▀█ ░░ █▀▀ █▄█ ▄█ ░█░ █ █░▀█ ▄█ ░█░ █▀█ █▄▄ █▄▄

version: 1.2.0release"
}

tty_setup(){
    echo "Выберите оболочку"
}

if [ $(ps ax | grep $$ | grep tty | awk '{ print $2 }') ]; then
    tty_setup
fi

PS3="Выберите пункт меню: "

cas
select start in Репозитории Зеркала Оптимизация Настройка\ служб ПО AUR Драйвера Ядра Звуковой\ сервер Игровое\ ПО Шрифты SELinux Выход; do
    
    case $start in
        Репозитории)
            sh ./modules/repos.sh # готово на 100%
            cas
        ;;
        
        Зеркала)
            sh ./modules/mirrors.sh # готово на 100%
            cas
        ;;
        
        Оптимизация)
            sh ./modules/optimizations.sh # готово на 100%
            #cas
        ;;
        
        Настройка\ служб)
            sh ./modules/services.sh # готово на 100%
            cas
        ;;
        
        ПО)
            sh ./modules/software.sh # готово на 100%
            cas
        ;;
        
        AUR)
            sh ./modules/aur.sh # готово на 100%
            cas
        ;;
        
        Драйвера)
            sh ./modules/drivers.sh # готово на 100%
            #cas
        ;;
        
        Ядра)
            sh ./modules/kernels.sh # готово на 100%
            cas
        ;;
        
        #ZSH)
        #    sh ./modules/zsh.sh # готово на 100%
            #cas
        #;;
        
        Звуковой\ сервер)
            sh ./modules/sound-server.sh # готово на 100%
            cas
        ;;
        
        Игровое\ ПО)
            sh ./modules/games.sh
            cas
        ;;
        
        Шрифты)
            sh ./modules/fonts.sh
            cas
        ;;

        SELinux)
            sh .modules/selinux.sh
            cas
        ;;
        
        Выход)
            echo "Don't forget to restart the system! Thank you for using the script"
            break
        ;;
        
        *)
        echo "Invalid option '$REPLY'";;
    esac
done
